import React, { useEffect, useState } from 'react'
import Image from './children/image'
import LikeButton from './children/like_button'
import styles from './styles'

const App = () => {
  const[data, setData] = useState(null);

  useEffect(()=> {
    fetch('data.json')
    .then((res) => {
      if(!res.ok){
        throw new Error('Issue getting data')
      }
      return res.json();
    })
    .then((data) => {
      // console.log({data});
      setData(data);
    })
  }, [])

  return (
    <main style={styles.main}>
      <div style={styles.image}>
        {data ? <Image data={data} /> : 'Couldn\'t get image'}
      </div>
      <div style={styles.text}>
        <p>User block</p>
        <p>Comments block</p>
        <LikeButton />
      </div>
    </main>
  )
}

export default App
